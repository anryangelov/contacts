from flask import Flask

from project_flask.contacts.routes import bp as contacts_bp
from project_flask.contacts.routes import db as contacts_db


def create_app(test_config_dict=None):
    app = Flask(__name__)

    app.config.from_envvar('CONFIG_PATH')

    if test_config_dict:
        app.config.update(**test_config_dict)

    contacts_db.init_app(app)

    app.register_blueprint(contacts_bp)

    return app
