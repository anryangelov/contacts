from flask import Blueprint
from flask import jsonify
from flask import request

from project_flask.contacts.models import db, row2dict
from project_flask.contacts.models import Contact


bp = Blueprint('contacts', __name__, url_prefix='/contacts')


def not_found():
    message = {
        'status': 404,
        'message': 'Not Found'
    }
    return jsonify(message), 404


def add_contact(contact):
    contact = Contact(**contact)
    db.session.add(contact)
    db.session.commit()
    return contact


@bp.route('/', methods=('POST',))
def save_contact():
    contact_params = request.get_json()
    contact = add_contact(contact_params)
    return jsonify(row2dict(contact))


@bp.route('/', methods=('GET', ))
def list_contacts():
    contacts = Contact.query.all()
    contacts = [row2dict(contact) for contact in contacts]
    return jsonify(contacts)


@bp.route('/username/<username>', methods=('GET',))
def get_contact(username):
    contact = Contact.query.filter(
        Contact.username == username).first()
    if not contact:
        return not_found()

    return jsonify(row2dict(contact))


@bp.route('/<int:contact_id>', methods=('PUT',))
def update_contact(contact_id):
    updated_contact = request.get_json()
    updated_id = db.session.query(Contact).filter(
        Contact.id == contact_id).update(updated_contact)
    if not updated_id:
        return not_found()

    return '', 204


@bp.route('/<int:contact_id>', methods=('PUT',))
def delete_contact(contact_id):
    return '', 204
