from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def row2dict(row):
    return {column.name: str(getattr(row, column.name))
            for column in row.__table__.columns}


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(), unique=True, nullable=False)
    first_name = db.Column(db.String(), nullable=False)
    sername = db.Column(db.String(), nullable=False)
    email = db.Column(db.String())

    def __repr__(self):
        return "<Contact(username='{}', first_name='{}', sername='{}')>".format(
            self.username, self.first_name, self.sername)
