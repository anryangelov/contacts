from project_flask.contacts.models import db, Contact, row2dict


contact1 = {
    'username': 'anryangelov',
    'first_name': 'Angel',
    'sername': 'Angelov',
    'email': 'anry.angelov@abv.bg'}

contact2 = {
    'username': 'foo',
    'first_name': 'fooname',
    'sername': 'foosername',
    'email': 'foo_mail'}


def test_post_contact(client):
    sent_contact = contact1.copy()
    response = client.post('/contacts/', json=sent_contact)

    assert response.status_code == 200

    db_contacts = Contact.query.all()
    assert len(db_contacts) == 1

    db_contact = db_contacts[0]
    sent_contact['id'] = '1'
    assert sent_contact == row2dict(db_contact)


def test_list_contacts(client):
    sent_contact1 = contact1.copy()
    sent_contact2 = contact2.copy()
    db.session.add(Contact(**sent_contact1))
    db.session.add(Contact(**sent_contact2))
    db.session.commit()

    response = client.get('/contacts/')

    assert response.status_code == 200

    sent_contact1['id'] = '1'
    sent_contact2['id'] = '2'

    assert response.json == [sent_contact1, sent_contact2]


def test_get_contact(client):
    db_contact1 = contact1.copy()
    db_contact2 = contact2.copy()
    db.session.add(Contact(**db_contact1))
    db.session.add(Contact(**db_contact2))
    db.session.commit()

    response = client.get('/contacts/username/{}'.format(db_contact1['username']))

    assert response.status_code == 200
    db_contact1['id'] = '1'
    assert response.json == db_contact1


def test_get_contact_not_found(client):
    response = client.get('contacts/username/{}'.format(contact1['username']))

    assert response.status_code == 404
    assert response.json['status'] == 404


def test_update_contact(client):
    db_contact1 = contact1.copy()
    db_contact2 = contact2.copy()
    db.session.add(Contact(**db_contact1))
    db.session.add(Contact(**db_contact2))
    db.session.commit()

    updated_contact = contact1.copy()
    not_updated_contact = contact2.copy()
    updated_contact['email'] = 'thenewmail@abv.bg'

    response = client.put('/contacts/1', json=updated_contact)

    updated_contact['id'] = '1'
    not_updated_contact['id'] = '2'

    assert response.status_code == 204

    got_contacts = Contact.query.all()

    assert len(got_contacts) == 2
    assert row2dict(got_contacts[0]) == updated_contact
    assert row2dict(got_contacts[1]) == not_updated_contact


def test_update_contact_not_found(client):
    response = client.put('/contacts/1000', json=contact1)
    assert response.status_code == 404
    assert response.json['status'] == 404


def delete_contact(client):
    db.session.add(Contact(contact1))
    db.session.commit()

    response = client.delete('contacts/1')

    assert response.status_code == 204
    assert not Contact.query.all()
