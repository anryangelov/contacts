import pytest

from project_flask import create_app
from project_flask.contacts.models import db as contacts_db


@pytest.fixture
def client():
    config = {
        'SQLALCHEMY_DATABASE_URI': 'sqlite:///:memory:',
        'SQLALCHEMY_TRACK_MODIFICATIONS': False,
        'testing': True
    }

    app = create_app(config)
    app.app_context().push()

    contacts_db.create_all()

    yield app.test_client()

    contacts_db.session.remove()
    contacts_db.drop_all()
