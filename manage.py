from flask_migrate import Migrate
from flask_migrate import MigrateCommand
from flask_script import Manager
from project_flask import create_app
from project_flask.contacts.models import db


app = create_app()

migrate = Migrate(app, db, directory='project_flask/migrations')

manager = Manager(app)
manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
